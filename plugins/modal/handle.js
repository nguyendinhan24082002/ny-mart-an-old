import layout from '@/plugins/modal/layout'

const VModal = {
  install(Vue) {
    this.EventBus = new Vue()

    Vue.components('v-modal', layout)

    Vue.prototype.$modal = {
      open(params) {
        VModal.EventBus.$emit('open', params)
      },
      close(params) {
        VModal.EventBus.$emit('close', params)
      },
    }
  },
}

export default VModal
