import Vue from 'vue'
import Element from 'element-ui'

// or
import {
  Select,
  Button,
  // ...
} from 'element-ui'

Vue.use(Element)

Vue.component(Select.name, Select)
Vue.component(Button.name, Button)
